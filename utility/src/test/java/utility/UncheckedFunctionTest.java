package utility;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utility.UncheckedFunction.UncheckedFunctionException;

import static org.junit.jupiter.api.Assertions.*;

public class UncheckedFunctionTest {

    private UncheckedFunction<String, Integer> uncheckedFunction;

    @BeforeEach
    public void setUp() {
        this.uncheckedFunction = Integer::valueOf;
    }

    @Test
    public void apply() {
        Integer expected = 0;
        Integer actual = uncheckedFunction.apply("0");

        assertEquals(expected, actual);
    }

    @Test
    public void uncheckedApply() {
        String expectedMessage = "WRONG_INPUT";

        UncheckedFunctionException exception = assertThrows(UncheckedFunctionException.class,
                () -> uncheckedFunction.apply(expectedMessage));

        assertTrue(exception.getMessage().contains(expectedMessage));
    }
}
