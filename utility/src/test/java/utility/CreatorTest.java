package utility;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreatorTest {

    //"AAA" Pattern.
    @Test
    public void description() {

        //"A" - Arrange
        String creatorName = "Bohdan";
        String expectedDescription = "Hello, I'm Bohdan - creator of ImageGenerator!";

        //"A" - Act
        Creator creator = new Creator(creatorName);

        //"A" - Assert
        assertEquals(creator.getDescription(), expectedDescription);
    }

}