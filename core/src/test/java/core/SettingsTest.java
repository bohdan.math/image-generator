package core;

import domain.InformationalImage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static core.Settings.MAX_EXPECTED_COLUMNS_COUNT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class SettingsTest {

    private Settings settings = new BasicSettings();
    private InformationalImage incomeImage = mock(InformationalImage.class);

    @BeforeEach
    public void setUp() {

        //set fields
        settings.setIncomeImage(incomeImage);
        settings.setPatterns(new HashMap<>());
        settings.setExpectedColumnsCount(MAX_EXPECTED_COLUMNS_COUNT / 2);
        settings.setIncomeImageName("image-file-name.jpg");

        //mock actions
        when(incomeImage.getWidth()).thenReturn(100);
        when(incomeImage.getHeight()).thenReturn(200);
        when(incomeImage.getSubImage(anyInt(), anyInt(), anyInt(), anyInt()))
                .thenReturn(new InformationalImage(1, 1, InformationalImage.TYPE_INT_RGB));
    }

    @Test
    public void getImage() {
        assertNotNull(settings.getIncomeImage());
    }

    @Test
    public void getPatterns() {
        assertNotNull(settings.getPatterns());
    }

    @Test
    public void getExpectedColumnsCount() {
        assertEquals(MAX_EXPECTED_COLUMNS_COUNT / 2, settings.getExpectedColumnsCount());
    }

    @Test
    public void getImageWidth() {
        assertEquals(100, settings.getImageWidth());
        verify(incomeImage, times(1)).getWidth();
    }

    @Test
    public void getImageHeight() {
        assertEquals(200, settings.getImageHeight());
        verify(incomeImage, times(1)).getHeight();
    }

    @Test
    public void getSubImage() {
        InformationalImage subImage = settings.getSubImage(1, 1, 1, 1);

        assertNotNull(subImage);
        verify(incomeImage, times(1)).getSubImage(eq(1), eq(1), eq(1), eq(1));
        assertEquals(1, subImage.getWidth());
        assertEquals(1, subImage.getHeight());
    }

    @Test
    public void getImageFileName() {
        assertEquals("image-file-name.jpg", settings.getIncomeImageName());
    }
}