package system;


import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ResourceTest {

    private Resource resource;

    @Test
    public void readCommonsImageByteArrays() throws Exception {
        this.resource = new Resource(get("images/colors"));
        long patternsCount = 24;

        Stream<byte[]> commons = resource.asByteArrays();

        assertNotNull(commons);
        assertEquals(commons.count(), patternsCount);
    }

    @Test
    public void readFlagsImageByteArrays() throws Exception {
        this.resource = new Resource(get("images/flags"));
        long patternsCount = 196;

        Stream<byte[]> flags = resource.asByteArrays();

        assertNotNull(flags);
        assertEquals(flags.count(), patternsCount);
    }

    @Test
    public void readPlainsImageByteArrays() throws Exception {
        this.resource = new Resource(get("images/plains"));
        long patternsCount = 3;

        Stream<byte[]> plains = resource.asByteArrays();

        assertNotNull(plains);
        assertEquals(plains.count(), patternsCount);
    }

    private Path get(String resourceName) throws URISyntaxException {
        return Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(resourceName)).toURI());
    }
}