package domain;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static domain.InformationalColor.*;
import static org.junit.jupiter.api.Assertions.*;

public class InformationalImageTest {

    private InformationalImage image;


    @BeforeEach
    public void setUp() {
        this.image = new InformationalImage(streamOf("images/testable/4x4.jpg")); // 32 x 32 (px)
    }

    @Test
    public void informationalImageMadeOfByteArray() {
        InformationalImage informationalImage = new InformationalImage(image.asByteArray());
        assertNotNull(informationalImage);
    }

    @Test
    public void informationalImageMadeOfInputStream() {
        InformationalImage informationalImage = new InformationalImage(image.asStream());
        assertNotNull(informationalImage);
    }

    @Test
    public void allowedWidth() {
        assertTrue(image.widthLessThan(33));
        assertTrue(image.widthMoreThan(0));
    }

    @Test
    public void allowedHeight() {
        assertTrue(image.heightLessThan(33));
        assertTrue(image.heightMoreThan(0));
    }

    @Test
    public void informationalImageAsStream() {
        assertNotNull(image.asStream());
    }

    @Test
    public void informationalImageAsBytes() {
        assertNotNull(image.asByteArray());
    }

    @Test
    public void whiteAveragedColor() {
        this.image = new InformationalImage(streamOf("images/testable/1-white.jpg"));
        InformationalColor white = image.averagedColor();
        assertEquals(WHITE, white);
    }

    @Test
    public void grayAveragedColor() {
        this.image = new InformationalImage(streamOf("images/testable/2-gray.jpg"));
        InformationalColor gray = image.averagedColor();
        assertEquals(GRAY, gray);
    }

    @Test
    public void blackAveragedColor() {
        this.image = new InformationalImage(streamOf("images/testable/3-black.jpg"));
        InformationalColor black = image.averagedColor();
        assertEquals(BLACK, black);
    }

    @Test
    public void getSubImage() {

        assertTrue(image.getSubImage(8, 0, 8, 8).averagedColor().almostEqualTo(WHITE));
        assertTrue(image.getSubImage(0, 8, 8, 8).averagedColor().almostEqualTo(WHITE));
        assertTrue(image.getSubImage(0, 0, 8, 8).averagedColor().almostEqualTo(BLACK));
        assertTrue(image.getSubImage(8, 8, 8, 8).averagedColor().almostEqualTo(BLUE));

        assertTrue(image.getSubImage(16, 0, 8, 8).averagedColor().almostEqualTo(GREEN));
        assertTrue(image.getSubImage(24, 0, 8, 8).averagedColor().almostEqualTo(WHITE));
        assertTrue(image.getSubImage(16, 8, 8, 8).averagedColor().almostEqualTo(WHITE));
        assertTrue(image.getSubImage(24, 8, 8, 8).averagedColor().almostEqualTo(RED));

        assertTrue(image.getSubImage(0, 16, 8, 8).averagedColor().almostEqualTo(RED));
        assertTrue(image.getSubImage(8, 16, 8, 8).averagedColor().almostEqualTo(WHITE));
        assertTrue(image.getSubImage(0, 24, 8, 8).averagedColor().almostEqualTo(WHITE));
        assertTrue(image.getSubImage(8, 24, 8, 8).averagedColor().almostEqualTo(GREEN));

        assertTrue(image.getSubImage(16, 16, 8, 8).averagedColor().almostEqualTo(BLUE));
        assertTrue(image.getSubImage(24, 16, 8, 8).averagedColor().almostEqualTo(WHITE));
        assertTrue(image.getSubImage(16, 24, 8, 8).averagedColor().almostEqualTo(WHITE));
        assertTrue(image.getSubImage(24, 24, 8, 8).averagedColor().almostEqualTo(BLACK));

        assertFalse(image.getSubImage(24, 24, 8, 8).averagedColor().almostEqualTo(WHITE));
    }

    @Test
    public void resizeTo() {
        int width = 250;
        int height = 250;
        InformationalImage resized = image.resizeTo(width, height);

        assertEquals(width, resized.getWidth());
        assertEquals(height, resized.getHeight());
    }

    private InputStream streamOf(String pathToImage) {
        return getClass().getClassLoader().getResourceAsStream(pathToImage);
    }
}