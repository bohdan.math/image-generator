package exception;

import core.BasicImageGenerator;
import core.BasicSettings;
import core.ImageGenerator;
import domain.InformationalImage;
import org.junit.jupiter.api.Test;

import static domain.InformationalImage.TYPE_INT_RGB;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MatrixSizeExceptionTest {

    @Test
    public void exceptionMessage() {
        ImageGenerator imageGenerator = new BasicImageGenerator();
        imageGenerator.setSettings(new BasicSettings() {{
            setIncomeImage(new InformationalImage(1, 1, TYPE_INT_RGB));
            setExpectedColumnsCount(10);
        }});

        MatrixSizeException exception = assertThrows(
                MatrixSizeException.class,
                imageGenerator::generateImage
        );

        assertTrue(exception.getMessage()
                .contains("Count of expected columns (is 10) could not be more than image width (is 1)"));
    }
}