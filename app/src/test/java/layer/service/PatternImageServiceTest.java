package layer.service;

import domain.InformationalColor;
import domain.InformationalImage;
import layer.repository.PatternImageRepository;
import model.PatternType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import system.ResourceReader;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.stream.Stream;

import static model.PatternType.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PatternImageServiceTest {

    @Autowired
    private PatternImageService patternImageService;

    @Autowired
    private PatternImageRepository patternImageRepository;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outContent));

        final byte[] imageByteArray = new ResourceReader().readAllIn("images/plains/").asByteArrays()
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Oops, something WRONG happens in your test!"));

        when(patternImageRepository.getCommons()).thenReturn(Stream.of(
                new InformationalImage(imageByteArray),
                new InformationalImage(imageByteArray),
                new InformationalImage(imageByteArray)));

        when(patternImageRepository.getFlags()).thenReturn(Stream.of(
                new InformationalImage(imageByteArray),
                new InformationalImage(imageByteArray)));

        when(patternImageRepository.getPlains()).thenReturn(Stream.of(
                new InformationalImage(imageByteArray)));
    }

    @Test
    public void cacheAndGetAllPatterns() {
        patternImageService.cacheAllPatterns();
        Map<PatternType, Map<InformationalColor, InformationalImage>> allPatterns = patternImageService.getAllPatterns();

        assertEquals(3, allPatterns.size());
        assertEquals(1, allPatterns.get(COMMONS).size());
        assertEquals(1, allPatterns.get(FLAGS).size());
        assertEquals(1, allPatterns.get(PLAINS).size());
    }

    @Test
    public void twoSameAveragedColors() {
        patternImageService.cacheAllPatterns();
        Map<PatternType, Map<InformationalColor, InformationalImage>> allPatterns = patternImageService.getAllPatterns();

        assertNotNull(allPatterns);
        assertTrue(outContent.toString().contains("Two same averaged colors:"));
    }

}