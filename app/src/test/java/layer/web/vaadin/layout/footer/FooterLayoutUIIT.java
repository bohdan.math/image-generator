package layer.web.vaadin.layout.footer;

import layer.web.vaadin.ImageGeneratorPageObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.title;
import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ContextConfiguration(locations = {
        "classpath*:layer/web/vaadin/UIIT-context.xml"
})
@ExtendWith(SpringExtension.class)
public class FooterLayoutUIIT {

    @Autowired
    private ImageGeneratorPageObject imageGeneratorPage;

    @BeforeEach
    public void setUp() throws Exception {
        open(imageGeneratorPage.url());
    }

    @Test
    public void linkedInLink() {
        imageGeneratorPage.linkedInLink().click();

        var title = title();
        assertTrue(requireNonNull(title).contains("LinkedIn"), title);
    }
}