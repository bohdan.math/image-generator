package layer.web.vaadin.layout.header;

import layer.web.vaadin.ImageGeneratorPageObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.title;
import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ContextConfiguration(locations = {
        "classpath*:layer/web/vaadin/UIIT-context.xml"
})
@ExtendWith(SpringExtension.class)
public class HeaderLayoutUIIT {

    @Autowired
    private ImageGeneratorPageObject imageGeneratorPage;

    @BeforeEach
    public void setUp() throws Exception {
        open(imageGeneratorPage.url());
    }

    @Test
    public void codacyLink() {
        imageGeneratorPage.codacyLink().click();
        assertTrue(requireNonNull(title()).contains("Codacy"));
    }

    @Test
    public void gitLabLink() {
        imageGeneratorPage.gitLabLink().click();
        assertTrue(requireNonNull(title()).contains("Bohdan Stepanov / image-generator · GitLab"));
    }
}