package layer.web.vaadin;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import layer.web.vaadin.additional.Reference;
import org.openqa.selenium.By;

import java.net.URL;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static layer.web.vaadin.layout.Tabs.GALLERY;
import static layer.web.vaadin.layout.Tabs.GENERATOR;
import static layer.web.vaadin.layout.footer.FooterLayout.LINKED_IN_LINK_ID;
import static layer.web.vaadin.layout.header.HeaderLayout.CODACY_LINK_ID;

public class ImageGeneratorPageObject {

    private String protocol;
    private String host;
    private Integer port;
    private String route;

    public ImageGeneratorPageObject(String browser, String protocol, String host, Integer port, String route) {
        Configuration.timeout = 20000;//ms TODO: fix timeout wait
        Configuration.browser = browser;
        Configuration.headless = true;

        this.protocol = protocol;
        this.host     = host;
        this.port     = port;
        this.route    = route;
    }

    public URL url() throws Exception {
        return new URL(protocol, host, port, route);
    }

    public SelenideElement generatorTab() {
        return $(byText(GENERATOR));
    }

    public SelenideElement galleryTab() {
        return $(byText(GALLERY));
    }

    public SelenideElement codacyLink() {
        return $(By.id(CODACY_LINK_ID));
    }

    public SelenideElement gitLabLink() {
        return $(By.linkText(Reference.GITLAB.link().getCaption()));
    }

    public SelenideElement linkedInLink() {
        return $(By.id(LINKED_IN_LINK_ID));
    }
}
