package layer.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PatternImageRepositoryTest {

    @Autowired
    private PatternImageRepository repository;

    @Test
    public void getCommons() {
        long three = 3;
        assertEquals(three, repository.getCommons().count());
    }

    @Test
    public void getFlags() {
        long two = 2;
        assertEquals(two, repository.getFlags().count());
    }

    @Test
    public void getPlains() {
        long one = 1;
        assertEquals(one, repository.getPlains().count());
    }
}