package layer.web.vaadin.additional;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Link;

public enum Reference {

    CODACY {
        @Override
        public Link link() {
            var codacyLink = link("https://app.codacy.com/gl/bohdan.math/image-generator/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade");
            codacyLink.setIcon(new ExternalResource("https://app.codacy.com/project/badge/Grade/eb0c8d3472e6413ebd0138943b15a546"));
            return codacyLink;
        }
    },

    GITLAB {
        @Override
        public Link link() {
            var gitLabLink = link("https://gitlab.com/bohdan.math/image-generator");
            gitLabLink.setCaption("Fork Me On GitLab");
            gitLabLink.setStyleName("gitlab-link");
            return gitLabLink;
        }
    },

    LINKED_IN {
        @Override
        public Link link() {
            var linkedInLink = link("https://www.linkedin.com/in/bohdan-math-stepanov/");
            linkedInLink.setCaption("Linked In");
            return linkedInLink;
        }
    };

    public abstract Link link();

    protected Link link(String sourceURL) {
        return new Link(null, new ExternalResource(sourceURL));
    }
}
