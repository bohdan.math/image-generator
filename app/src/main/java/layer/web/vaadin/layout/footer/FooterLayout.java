package layer.web.vaadin.layout.footer;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import info.AppBuildInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;

import static layer.web.vaadin.additional.Reference.LINKED_IN;

@SpringComponent
@Scope("session")
public class FooterLayout extends VerticalLayout {

    public static final String LINKED_IN_LINK_ID = "linked-in-link-id";

    @Autowired
    private AppBuildInformation appBuildInformation;

    @PostConstruct
    public void postConstruct() {

        var linkedInLink = LINKED_IN.link();

        linkedInLink.setId(LINKED_IN_LINK_ID);

        HorizontalLayout footerLinksLayout = new HorizontalLayout(
                linkedInLink
        );

        HorizontalLayout appBuildInformationLayout = new HorizontalLayout(
                new Label("branch: " + appBuildInformation.getBranchName()),
                new Label("revision: " + appBuildInformation.getCommitHash())
        );

        addComponents(footerLinksLayout, appBuildInformationLayout);

        setComponentAlignment(footerLinksLayout, Alignment.MIDDLE_CENTER);
        setComponentAlignment(appBuildInformationLayout, Alignment.MIDDLE_RIGHT);
        setSizeFull();
    }
}
