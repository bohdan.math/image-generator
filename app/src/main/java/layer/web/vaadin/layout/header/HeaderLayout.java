package layer.web.vaadin.layout.header;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;

import static layer.web.vaadin.additional.Reference.CODACY;
import static layer.web.vaadin.additional.Reference.GITLAB;

@SpringComponent
@Scope("session")
public class HeaderLayout extends HorizontalLayout {

    public static final String CODACY_LINK_ID = "codacy-link-id";
    public static final String GITLAB_LINK_ID = "gitlab-link-id";

    @PostConstruct
    public void postConstruct() {

        Link codacyLink = CODACY.link();
        Link gitLabLink = GITLAB.link();

        codacyLink.setId(CODACY_LINK_ID);
        gitLabLink.setId(GITLAB_LINK_ID);

        addComponents(codacyLink, gitLabLink);

        setComponentAlignment(codacyLink, Alignment.TOP_LEFT);
        setComponentAlignment(gitLabLink, Alignment.TOP_RIGHT);
    }
}
