package info;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppBuildInformation {

    /*artifact info*/
    private String groupId;
    private String artifactId;
    private String version;

    /*git info*/
    private String branchName;
    private String commitHash;
}
